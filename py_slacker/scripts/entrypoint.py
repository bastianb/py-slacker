import click
import sys

from py_slacker import PySlacker

@click.command()
@click.option('--address', '-a', required=True,
    help='Address of the slack channel to post the message.'
    'Format: https://hooks.slack.com/services/Txxx/Byyy/Hzzz'
)
@click.option('--channel', '-c', required=True,
    help='The channel to post the message to.'
)
@click.option('--username', '-u', required=True,
    help='The username to use to post the message.'
)
@click.option('--message', '-m', required=True,
    help='The message to post to the channel.'
)
@click.option('--icon-emoji', '-i',
    help='The icon emoji to use to post message.'
    default=':ghost:'
)
def send_message(address, channel, username, message, icon_emoji):
    """ Send a message to a channel
    """
    slacker = PySlacker(
        address=address,
        username=username,
        icon_emoji=icon_emoji
    )
    result = slacker.send_message(channel, message)
    if result.text == 'ok':
        sys.exit(0)
    click.echo('ERROR: {error}'.format(error=result.text))
    sys.exit(1)


if __name__ == '__main__':
    cmd()
