__version__ = '0.0.1.dev0'

import os
import requests
import json


class PySlacker(object):
    def __init__(self, address, username, icon_emoji=':ghost:'):
        self.address = address
        self.username = username
        self.icon_emoji = icon_emoji

    def send_message(self, channel, text, username=None, icon_emoji=None):
        if not channel or not text or not self.username:
            raise AttributeError('Channel, Text, Username can not be empty.')
        return requests.post(
            self.address,
            json={
                'channel': '{channel}'.format(channel=channel),
                'username': '{username}'.format(
                    username=username or self.username
                ),
                'text': '{text}'.format(text=text),
                'icon_emoji': '{icon_emoji}'.format(
                    icon_emoji=icon_emoji or self.icon_emoji
                )
            }
        )
