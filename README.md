py-slacker

Library used to send slack notifications

Install:
pip install .

Usage:
wi-slacker --address='https://hooks.slack.com/services/Txxx/Byyy/Hzzz' --channel='#general' --username=user --message=test
