import os
import re

from setuptools import find_packages
from setuptools import setup


def read_file(filename):
    """Open and a file, read it and return its contents."""
    path = os.path.join(os.path.dirname(__file__), filename)
    with open(path) as f:
        return f.read()


def get_version():
    """Extract and return version number from the packages '__init__.py'."""
    init_path = os.path.join('py_slacker', '__init__.py')
    content = read_file(init_path)
    match = re.search(r"__version__ = '([^']+)'", content, re.M)
    version = match.group(1)
    return version


setup(
    name='py-slacker',
    version=get_version(),
    author='Bretagne Bastian',
    author_email='bretagne.bastian@gmail.com',
    description='Py-Slacker helper tool for slack notifications',
    url='https://bitbucket.org/bastianb/py-slacker',
    install_requires=read_file('requirements.txt'),
    packages=find_packages(include=('py_slacker*',)),
    include_package_data=True,
    entry_points={
        'console_scripts': ['py-slacker=py_slacker.scripts.entrypoint:send_message'],
    }
)
